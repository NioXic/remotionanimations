import {Composition} from 'remotion';
import {KAYAK} from './components/templates/KAYAK/KAYAK';
import {KayakNew} from './components/templates/KAYAK2/KayakNew';
import {ThisIsVancouver} from './components/templates/ThisIsVancouver/ThisIsVancouver';
import {IndulgantEscapes} from './components/templates/IndulgantEscapes/IndulgantEscapes';
import {BestVacationResort} from "./components/templates/BestVacationResort/BestVacationResort";
// Importing global files
import "../src/config/fonts/font.css";
import 'bootstrap/dist/css/bootstrap.min.css';
// Import mainConfig from "./config/mainConfig";

// Each <Composition> is an entry in the sidebar!
export const RemotionVideo: React.FC = () => {
    /**
     * ToDO: make this code usable, read data from one file and apply (this needs to support all kind of data).
     */
    // let videoHeight = 0;
    // let videoWidth = 0;
    // if (mainConfig.videoFormat === "square") {
    //     videoWidth = 1080;
    //     videoHeight = 1080;
    // }
    // if (mainConfig.videoFormat === "vertical") {
    //     videoWidth = 720;
    //     videoHeight = 1280;
    // }
    // if (mainConfig.videoFormat === "horizontal") {
    //     videoWidth = 1280;
    //     videoHeight = 720;
    // }

    return (
        <>
            <Composition
                // You can take the "id" to render a video:
                // npx remotion render src/index.tsx <id> out/video.mp4
                id="MainComponent"
                component={BestVacationResort}
                durationInFrames={130}
                fps={30}
                width={1080}
                height={1600}
                // You can override these props for each render:
                // https://www.remotion.dev/docs/parametrized-rendering
            />
        </>
    );
};
