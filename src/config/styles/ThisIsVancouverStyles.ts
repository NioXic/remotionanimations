import React from "react";
import configData from "../../components/templates/ThisIsVancouver/config";

const starterTextCSS: React.CSSProperties = {
    fontFamily: `${configData.fonts.primaryFont}`,
    textAlign: "center",
    width: '65%',
    padding: "25px",
    fontSize: "3rem",
    color: configData.colors.textPrimaryColor,
};
const mainPageCss: React.CSSProperties = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: '100%',
    height: "35%",
    backgroundColor: configData.colors.primaryColor,
    textAlign: "center",
};
const displayFlexFillCenter: React.CSSProperties = {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    width: "100%",
}
const endingMainText: React.CSSProperties = {
    fontSize: "2.5rem",
    textAlign: "center",
    paddingBottom: "20px",
    fontWeight: "bold",
    color: configData.colors.textBasicColor,
    fontFamily: configData.fonts.secondaryFont,
}
const companyNameFirstCSS: React.CSSProperties = {
    fontSize: "2.5rem",
    fontWeight: "bold",
    textAlign: "center",
    color: configData.colors.primaryColor,
    fontFamily: configData.fonts.secondaryFont,
    padding: "5px 15px 5px 15px",
    borderRadius: "2px",
    backgroundColor: configData.colors.secondaryColor,
    boxShadow: `0px 0px 10px 1px ${configData.colors.textPrimaryColor}`
}
const companyNameSecondCSS: React.CSSProperties = {
    fontSize: "2.5rem",
    position: "absolute",
    fontWeight: "bold",
    color: configData.colors.textBasicColor,
    fontFamily: configData.fonts.secondaryFont,
    bottom: "25%",
    left: "42%"
}
const companyNameRotate180: React.CSSProperties = {
    color: configData.colors.textSecondaryColor,
    fontSize: "2.65rem",
    left: "-55%",
    top: "-8%",
    position: "absolute",
    transform: "rotate(180deg)"
}

export default {
    starterTextCSS,
    mainPageCss,
    displayFlexFillCenter,
    endingMainText,
    companyNameFirstCSS,
    companyNameSecondCSS,
    companyNameRotate180,
}