import React from "react";

const centeringRow: React.CSSProperties = {
    width: "100%",
    marginLeft: 0,
    alignItems: "center",
    justifyContent: "center",
}

const verticalCentering: React.CSSProperties = {
    display: 'flex',
    alignItems: "center",
}
const horizontalCentering: React.CSSProperties = {
    display: 'flex', justifyContent: "center",
}
const centering: React.CSSProperties = {
    display: 'flex',
    justifyContent :"center",
    alignItems:"center"

}
export default {
    centeringRow,
    horizontalCentering,
    verticalCentering,
    centering
}