import React from "react";
import configData from "../../components/templates/IndulgantEscapes/config";

const topBoxCss: React.CSSProperties = {
    width: '65%',
    height: "400px",
    padding: "25px",
    color: configData.colors.textPrimaryColor,
    backgroundColor: configData.colors.primaryColor,
};
const endTextCss: React.CSSProperties = {
    fontFamily: configData.fonts.secondaryFont,
    marginTop: '30%',
    textAlign: "center",
    fontSize: "2.5rem",
    width: '100%',
    borderBottom: `3px solid ${configData.colors.textPrimaryColor}`
};
const companyNameCss: React.CSSProperties = {
    fontFamily: configData.fonts.primaryFont,
    fontStyle: "italic",
    fontSize: 20,
};
const titleMainDivCss: React.CSSProperties = {
    textAlign: 'center',
    position: "relative",
    width: '100%',
    padding: "25px",
    color: configData.colors.textPrimaryColor,
    backgroundColor: configData.colors.secondaryColor,
};
const titleTextCss: React.CSSProperties = {
    fontFamily: configData.fonts.secondaryFont,
    fontSize: "2.7rem",
    width: '100%',
    backgroundColor: configData.colors.secondaryColor,
    color: configData.colors.textPrimaryColor,
    marginTop: "5px",
    textAlign: "center",
    left: 0,
};

const mainPageCss: React.CSSProperties = {
    position: 'absolute',
    width: '100%',
    height: "100%",
    backgroundColor: configData.colors.primaryColor,
    textAlign: "center",
};
const mainContainerCss: React.CSSProperties = {
    textAlign: "center",
    color: configData.colors.textPrimaryColor,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
    flexDirection: "column"
};
const endingTextCss: React.CSSProperties = {
    fontFamily: configData.fonts.secondaryFont,
    textAlign: "center",
    fontSize: "3.5rem",
    position: "relative",
    borderBottom: `3px solid ${configData.colors.textPrimaryColor}`
};
const endingCompanyNameCss: React.CSSProperties = {
    fontFamily: configData.fonts.primaryFont,
    fontStyle: "italic",
    fontSize: "1.5rem",
};


export default {
    topBoxCss,
    endTextCss,
    companyNameCss,
    titleMainDivCss,
    titleTextCss,
    mainPageCss,
    mainContainerCss,
    endingTextCss,
    endingCompanyNameCss,

}