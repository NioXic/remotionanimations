import React from "react";
import configData from "../../components/templates/KAYAK2/config";

const mainTextStyle2DTransform: React.CSSProperties = {
    textAlign: "center",
    fontSize: 100,
    color: configData.colors.textPrimaryColor,
    transform: "translate(-6px, -6px)",
    position: "absolute",
    zIndex:3000,
    fontWeight: "bold",
    fontFamily: configData.fonts.primaryFont,
};
const secondTextStyle2DTransform: React.CSSProperties = {
    textAlign: "center",
    display: 'inline-block',
    color: configData.colors.textSecondaryColor,
    fontSize: 100,
    fontWeight: "bold",
    zIndex:2800,
    fontFamily: configData.fonts.primaryFont,
}
const startTextContainer: React.CSSProperties = {
    textAlign: "center",
    color: configData.colors.textPrimaryColor,
    fontWeight: "bold",
    fontFamily: configData.fonts.primaryFont,
    zIndex:3000,
}
const textAlignCenter: React.CSSProperties = {
    textAlign: "center",
}
const mainTextContainer: React.CSSProperties = {
    textAlign: "center"
}
const textContainer: React.CSSProperties = {
    textAlign: "center",
    fontFamily: configData.fonts.primaryFont,
}
const companyNameText: React.CSSProperties = {
    padding: "15px 30px",
    backgroundColor: configData.colors.textSecondaryColor,
    bottom: "50%",
    zIndex:3000,
    marginLeft: "5px",
    marginRight: "5px",
}
const companyNameCharacter: React.CSSProperties = {
    zIndex:3000,
    fontSize: 85, fontWeight: "bold", color: configData.colors.textPrimaryColor
}
const centeringRow: React.CSSProperties = {
    width: "100%",
    marginLeft: 0,
    alignItems: "center",
    justifyContent: "center",
}

const verticalCentering: React.CSSProperties = {
    display: 'flex',
    alignItems: "center",
}
const horizontalCentering: React.CSSProperties = {
    display: 'flex', justifyContent: "center",
}

export default {
    mainTextStyle2DTransform,
    secondTextStyle2DTransform,
    startTextContainer,
    textAlignCenter,
    mainTextContainer,
    textContainer,
    companyNameText,
    companyNameCharacter,
    centeringRow,
    horizontalCentering,
    verticalCentering,
}