import React from "react";
// You can modify this css file to have different kind of effect (you just need to change the image url)
import "./FilmGrainStyle.css"

/**
 *#Film Grain Effect
 *## This effect will add the film grain to all of its children
 *### It uses a simple css class to make this effect.
 * @param children, (JSX Elements)
 * @constructor
 */
export const FilmGrain: React.FC<{
    children?:
        | React.ReactChild
        | React.ReactChild[]
}> = ({
    children,
     }) => {

    return (
        // Adding effect to this parent child
        <div className="filmGrain">
            {children}
        </div>
    );
};