import {AbsoluteFill, Video} from "remotion";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import {configData} from "./config/config";
import React, {CSSProperties} from "react";
import styles from "../../config/styles/KAYAKStyles";

/**
 * #Text 2D Effect
 * ## This effect will make a string in 2D
 * @param text: string that you want in 2D
 * @param key:Number: for react fabric.
 * @param baseCSS:CSSProperties: your main color, this should also contain the fontSize and other properties you want to apply to the first text
 * @param secondCSS:CSSProperties: your second color, this should also contain the fontSize and other properties you want to apply to the second text
 * @constructor
 */
export const Words2D: React.FC<{ text: string, key: number, baseCSS: CSSProperties, secondCSS: CSSProperties }> = ({
       text,
       key,
       baseCSS,
       secondCSS
   }) => {
    return (
        <div style={{
            marginRight: "3px",
            marginLeft: "3px",
            height: "100%",
            width: "100%",
            display: "flex",
            justifyContent: "center"
        }}>
            <div key={key} style={baseCSS}>
                {text}
            </div>
            <div key={key} style={secondCSS}>
                {text}
            </div>
        </div>
    );
};