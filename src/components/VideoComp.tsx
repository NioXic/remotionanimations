import {AbsoluteFill, Video} from "remotion";
import {interpolate, useCurrentFrame, useVideoConfig, spring} from "remotion";

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import {configData} from "./config/config";
import React from "react";
import {VolumeProp} from "remotion/dist/volume-prop";
// @ts-ignore
import {FadeInOutAnimation} from "./animations/FadeInOutAnimation";

/**
 *#Video Component
 * @param videoAddress: video Source address
 * @param videoStartFrom: video starts from (you can also not provide this field and it will start from begining)
 * @param videoEndAt: video ends at (you can also not provide this field and it will end at the end of the sequence or the time frame that you have used this component)
 * @constructor
 */
export const VideoComp: React.FC<{ videoAddress: string, videoStartFrom?: number, videoEndAt?: number }> = ({
        videoAddress,
        videoStartFrom,
        videoEndAt,

    }) => {
    /**
     * Saving this for future reference
    // const frame = useCurrentFrame();
    // const {durationInFrames, fps} = useVideoConfig();
    // // for smoother transition
    // const transition = spring({
    //     fps,
    //     from: 0,
    //     to: 1,
    //     frame,
    // });
    // //Fade out-in in every video change.
    // const opacity = interpolate(
    //     frame,
    //     [5, 15, durationInFrames - 10, durationInFrames],
    //     // v--v---v----------------------v
    //     [0.1, 1, 1, 0.1]
    // );
     */
    return (
        <AbsoluteFill>
            <Video src={videoAddress} volume={0}
                   startFrom={videoStartFrom}
                   endAt={videoEndAt}
            />
        </AbsoluteFill>
    );
};