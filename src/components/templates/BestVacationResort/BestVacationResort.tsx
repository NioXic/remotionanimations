import {
    AbsoluteFill, Audio, Img,
    Sequence,
} from 'remotion';
// import {VideoComp} from '../../VideoComp';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// reading from the basic config data
import configData from "./config";
// Reading from main config data
// import configData from "../../../config/mainConfig"
import {TranslateYAnimation} from "../../animations/TranslateYAnimation";
import React from "react";
import {FadeAnimation} from "../../animations/FadeAnimation";
import {TranslateXAnimation} from "../../animations/TranslateXAnimation";
import styles from "../../../config/styles/IndulgantEscapesStyles";
import globalStyles from "../../../config/styles/GlobalStyles";
import {Row, Col, Container} from 'react-bootstrap';


export const BestVacationResort: React.FC<{
// eslint-disable-next-line no-empty-pattern
}> = ({}) => {
    return (
        <AbsoluteFill >
            {/* Mapping the audio, so if the audio was more than 1 it could handle it. */}
            {/* {configData.sfx.map((audioAddress: string, i: number) => */}
            {/*     // For each 8 seconds change the audio */}
            {/* // i > 0 ? */}
            {/*     <Sequence key={i} from={i * 250}> */}
            {/* Trimming the video with passing two props */}
            <Audio
                src={configData.sfx[0].address}
                startFrom={configData.sfx[0].startsFrom}
                endAt={configData.sfx[0].endsAt}
            />
            {/* </Sequence> */}
            {/* )} */}
            {/* Mapping the footage, so if the footage was more than 1 it could handle it. */}
            <Sequence key={0} from={configData.footage[0].startsFrom}
                      durationInFrames={configData.footage[0].endsAt - configData.footage[0].startsFrom}>
                {/* Trimming the video with passing two props */}
                <AbsoluteFill>
                    <Img src={configData.footage[0].address}/>
                </AbsoluteFill>
            </Sequence>
            {/* Sequences can shift the time for its children! */}
            <Sequence from={0}
                      durationInFrames={configData.texts.startText.endsAt - configData.texts.startText.startsFrom}>
                <Container
                    style={{
                        fontFamily: configData.fonts.primaryFont,
                        width: "80%",
                        height: "30%",
                        marginTop: "10%",
                        justifyContent: "center",
                        display: "flex",
                        flexDirection: "column",
                        textAlign: 'center'
                    }}>
                    <span style={{fontSize: "3.5rem", fontWeight: "400", color: configData.colors.textPrimaryColor}}>
                        {configData.texts.startText.text}...
                    </span>
                    <span style={{fontSize: "4rem", fontWeight: "500", color: configData.colors.secondaryColor}}>
                        {configData.texts.middleText.text}
                    </span>
                    <span style={{fontSize: "5rem", fontWeight: "500", color: configData.colors.primaryColor}}>
                        {configData.texts.endText.text}
                    </span>
                </Container>
            </Sequence>
            <Sequence from={configData.texts.outroText.startsFrom}>
                <Container
                    style={{
                        width: "80%",
                        fontFamily: configData.fonts.primaryFont,
                        height: "30%",
                        marginTop: "10%",
                        justifyContent: "center",
                        display: "flex",
                        flexDirection: "column",
                        textAlign: 'center'
                    }}>
                    <span style={{fontSize: "2.7rem", fontWeight: "400", color: configData.colors.textPrimaryColor}}>
                        {configData.texts.outroText.text}
                    </span>
                    <span style={{fontSize: "5.5rem", fontWeight: "500", color: configData.colors.primaryColor}}>
                        {configData.texts.companyName.text}
                    </span>
                    <span style={{fontSize: "4rem", fontWeight: "500", color: configData.colors.secondaryColor}}>
                        {configData.texts.companyAddress.text}
                    </span>
                </Container>
            </Sequence>
        </AbsoluteFill>
    );
};
