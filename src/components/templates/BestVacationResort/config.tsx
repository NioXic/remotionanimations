export default {
    "savePath": "/tmp",
    "videoFormat": "vertical",
    "videoDuration": 220,
    // Array for looping easier
    "footage": [
        {
            "startsFrom": 0,
            "endsAt": 170,
            // "address": require("../../../assets/SFX/SFX1.mp3"),
            "address": require("../../../assets/images/familyImage.png"),
        }
        ,
    ],
    // Array for looping easier
    "sfx": [
        {
            "startsFrom": 4050,// If composition is 30fps, then it will start at 2:15
            "endsAt": 4300, // If composition is 30fps, then it will ends at 2:23s
            "address": require("../../../assets/SFX/SFX6.mp3"),
        }
    ],
    "texts": {
        // It's easier to provide duration instead of end frame
        "companyName": {
            "text": "David Walley's Resort",
            "startsFrom": 80,
            "endsAt": 170,
        },
        "companyAddress" :{
          "text": "IN GENOA, NEVADA!",
            "startFrom": 80,
            "endsAt": 170,
        },
        "startText": {
            "text": "NO NEED TO KEEP LOOKING",
            "startsFrom": 0,
            "endsAt": 80,
            "animationDuration": 90,
        },
        "middleText": {
            "text": "YOU ALREADY FOUND THE",
            "startsFrom": 0,
            "endsAt": 80,
            "animationDuration": 5,
        },
        "endText": {
            "text": "BEST RESORT NEAR LAKE TAHOE!",
            "startsFrom": 0,
            "endsAt": 80,
            "animationDuration": 60,
        },
        "outroText": {
            "text": "BOOK YOUR ~AMAZING~ VACATION AT",
            "startsFrom": 80,
            "endsAt": 170,
            "animationDuration": 60,
        },
    },
    "colors": {
        "primaryColor": "#D76418",
        "secondaryColor": "#403F4E",
        "textPrimaryColor": "#828793",
    },
    "fonts": {
        "primaryFont": "Bahnschrift",
        "secondaryFont": "MontserratMedium",
    }
};
