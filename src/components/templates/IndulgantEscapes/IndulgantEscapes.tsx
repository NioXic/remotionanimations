import {
    AbsoluteFill, Audio,
    Sequence,
} from 'remotion';
import {VideoComp} from '../../VideoComp';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// reading from the basic config data
import configData from "./config";
// Reading from main config data
// import configData from "../../../config/mainConfig"
import {TranslateYAnimation} from "../../animations/TranslateYAnimation";
import React from "react";
import {FadeAnimation} from "../../animations/FadeAnimation";
import {TranslateXAnimation} from "../../animations/TranslateXAnimation";
import styles from "../../../config/styles/IndulgantEscapesStyles";
import globalStyles from "../../../config/styles/GlobalStyles";
import {Row, Col} from 'react-bootstrap';


export const IndulgantEscapes: React.FC<{
// eslint-disable-next-line no-empty-pattern
}> = ({}) => {
    return (
        <AbsoluteFill>
            {/* Mapping the audio, so if the audio was more than 1 it could handle it. */}
            {/* {configData.sfx.map((audioAddress: string, i: number) => */}
            {/*     // For each 8 seconds change the audio */}
            {/* // i > 0 ? */}
            {/*     <Sequence key={i} from={i * 250}> */}
            {/* Trimming the video with passing two props */}
            <Audio
                src={configData.sfx[0].address}
                startFrom={configData.sfx[0].startsFrom}
                endAt={configData.sfx[0].endsAt}
            />
            {/* </Sequence> */}
            {/* )} */}
            {/* Mapping the footage, so if the footage was more than 1 it could handle it. */}
            {configData.footage.map((eachVideo, i: number) =>
                <Sequence key={i} from={eachVideo.startsFrom} durationInFrames={eachVideo.endsAt - eachVideo.startsFrom}>
                    {/* Trimming the video with passing two props */}
                    <VideoComp videoAddress={eachVideo.address}/>
                </Sequence>
            )}
            {/* Sequences can shift the time for its children! */}
            <Sequence from={0}>
                {/* <TopBox/> */}
                <Row style={{...globalStyles.centeringRow, alignItems: "start"}}>
                    <Col style={{justifyContent: "center", display: "flex"}}>
                        <TranslateYAnimation
                            mainContainerCss={styles.topBoxCss}
                            animationStartFrom={configData.texts.endText.startsFrom}
                            animationDamping={100}
                            animationDuration={configData.texts.endText.animationDuration}
                            translateYAxisFrom={0} translateYAxisTo={-400}>
                            <Row style={{width: "100%", marginLeft: 0, justifyContent: "center"}}>
                                    <span style={styles.endTextCss}>
                                        {configData.texts.endText.text}
                                    </span>
                            </Row>
                            <Row style={{
                                width: "100%",
                                marginLeft: 0,
                                justifyContent: "flex-end",
                                paddingTop: "2%"
                            }}>
                                <Col className="col-6" style={{display: "flex", justifyContent: "flex-end"}}>
                                        <span style={styles.companyNameCss}>
                                            {configData.texts.companyName.text}
                                        </span>
                                </Col>
                            </Row>
                        </TranslateYAnimation>
                    </Col>
                </Row>
                {/* Fade out animation */}
                <AbsoluteFill style={globalStyles.centering}>
                    <Row style={{width: '100%', alignItems: "center"}}>
                        <FadeAnimation mainContainerCss={styles.titleMainDivCss}
                                       animationStartFrom={configData.texts.startText.endsAt - 5}
                                       animationEndAt={configData.texts.startText.endsAt}
                                       opacityFrom={1} opacityTo={0}>
                                <span style={styles.titleTextCss}>
                                    {configData.texts.startText.text}
                                </span>
                            <Row style={{
                                position: "absolute",
                                top: "15%",
                                width: "100%",
                            }}>
                                <TranslateXAnimation animationDuration={configData.texts.middleText.animationDuration}
                                                     mainContainerCss={styles.titleTextCss}
                                                     animationDamping={25}
                                                     animationStartFrom={configData.texts.middleText.startsFrom}
                                                     translateXAxisFrom={-1080}
                                                     translateXAxisTo={0}>
                                    {configData.texts.middleText.text}
                                </TranslateXAnimation>
                            </Row>
                        </FadeAnimation>
                    </Row>
                </AbsoluteFill>
            </Sequence>
            <Sequence from={configData.texts.endText.startsFrom}>
                <AbsoluteFill style={{display: "flex", justifyContent: "center", alignItems: "center"}}>
                    <TranslateYAnimation
                        mainContainerCss={styles.mainPageCss}
                        animationStartFrom={0} animationDamping={20} animationDuration={50}
                        translateYAxisFrom={1920} translateYAxisTo={0}/>
                    <FadeAnimation mainContainerCss={styles.mainContainerCss} animationStartFrom={5}
                                   animationEndAt={20}
                                   opacityFrom={0} opacityTo={1}>
                        <span style={styles.endingTextCss}>
                            {configData.texts.endText.text}
                        </span>
                        <Row style={{
                            width: "80%", ...styles.endingTextCss,
                            border: "none",
                            justifyContent: "flex-end",
                            fontSize: "50px"
                        }}>
                            <Col className="col-4 col-xl-6">
                                    <span style={styles.endingCompanyNameCss}>
                                         {configData.texts.companyName.text}
                                    </span>
                            </Col>
                        </Row>
                    </FadeAnimation>
                </AbsoluteFill>
            </Sequence>
        </AbsoluteFill>
    );
};
