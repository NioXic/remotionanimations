export default {
    "savePath": "/tmp",
    "videoFormat": "vertical",
    "videoDuration": 220,
    // Array for looping easier
    "footage": [
        {
            "startsFrom": 0,
            "endsAt": 300,
            "address": require("../../../assets/footages/mainVideo.mp4"),
        }
        ,
    ],
    // Array for looping easier
    "sfx": [
        {
            "startsFrom": 4050,// If composition is 30fps, then it will start at 2:15
            "endsAt": 4300, // If composition is 30fps, then it will ends at 2:23s
            "address": require("../../../assets/SFX/SFX1.mp3"),
        }
    ],
    "texts": {
        // It's easier to provide duration instead of end frame
        "companyName": {
            "text": "Zebracat",
            "startsFrom": 0,
            "endsAt": 250,
        },
        "startText": {
            "text": "Something is included",
            "startsFrom": 0,
            "endsAt": 155,
            "animationDuration": 90,
        },
        "middleText": {
            "text": "Something else is private",
            "startsFrom": 90,
            "endsAt": 150,
            "animationDuration": 5,
        },
        "endText": {
            "text": "AI-Generated Videos",
            "startsFrom": 200,
            "endsAt": 250,
            "animationDuration": 60,
        },
    },
    "colors": {
        // primaryColor: "#3A3A39",
        // secondaryColor: "#6C6A40",
        // textPrimaryColor: "#FDFCFC",
        "primaryColor": "#293462",
        "secondaryColor": "#D61C4E",
        "textPrimaryColor": "#FEB139",
    },
    "fonts": {
        "primaryFont": "FranklinGothicHeavyRegular",
        "secondaryFont": "MontserratMedium",
    }
};
