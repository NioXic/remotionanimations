export default {
    "savePath": "/tmp",
    "videoFormat" : "vertical",
    "videoDuration": 220,

    // Array for looping easier
    "footage": [
        // Video timing: 1 sec every video
        {
            "startsFrom": 0,
            "endsAt": 30,
            "address": require("../../../assets/footages/squareFootage/1.mp4") // Use Require because of using local files
        },
        {
            "startsFrom": 30,
            "endsAt": 60,
            "address": require("../../../assets/footages/squareFootage/2.mp4")
        },
        {
            "startsFrom": 60,
            "endsAt": 90,
            "address": require("../../../assets/footages/squareFootage/3.mp4")
        },
        {
            "startsFrom": 90,
            "endsAt": 120,
            "address": require("../../../assets/footages/squareFootage/4.mp4")
        },
        {
            "startsFrom": 120,
            "endsAt": 170,
            "address": require("../../../assets/footages/squareFootage/5.mp4")
        }
    ],
    // Array for looping easier
    "sfx": [
        {
            "startsFrom": 240,
            // If composition is 30fps, then it will start at 5
            "endsAt": 460,
            // If composition is 30fps, then it will ends at 12s
            "address": require(
                "../../../assets/SFX/SFX5.mp3"
            )
        }
    ],
    "texts": {
        // It's easier to provide duration instead of end frame
        "companyName": {
            "text": "KAYAK",
            "startsFrom": 0,
            "endsAt": 30
        },
        "startText": {
            "text": "This summer make it one to remember",
            "startsFrom": 50,
            "endsAt": 135,
            "animationDuration": 45
        },
        "endText": {
            "text": "KAYAK",
            "startsFrom": 170,
            "endsAt": 200
        }
    },
    "colors": {
        "primaryColor": "#333C83",
        "secondaryColor": "#F24A72",
        "textPrimaryColor": "#FDAF75",
        "textSecondaryColor": "#F24A72"
        // PrimaryColor: "#FBEDD5",
        // secondaryColor: "#6C6A40",
        // textPrimaryColor: "#FFFFFFFF",
        // textSecondaryColor: "#F96528",
    },
    "fonts": {
        "primaryFont": "TTHovesRegular",
        "secondaryFont": "MontserratMedium"
    }
}


