import {
    AbsoluteFill, Audio, interpolate,
    Sequence, useCurrentFrame, useVideoConfig, Easing, spring, staticFile
} from 'remotion';
import {VideoComp} from '../../VideoComp';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// import {configData} from "./config";
import React from "react";

import {FilmGrain} from "../../effects/FilmGrain";
import styles from "../../../config/styles/KAYAK2Styles";
import globalStyles from "../../../config/styles/GlobalStyles";
import textAnimations from "../../animations/Fade2DAnimation"
import companyTitle from "../KAYAK/CompanyTitle"
import {Row, Container, Col, Fade} from 'react-bootstrap';
import "../../effects/FilmGrainStyle.css"
import {SearchIcon} from "../../../assets/SVG/SearchIcon";
import {FadeAnimation} from "../../animations/FadeAnimation";
import {EasingBezier} from "../../animations/EasingBezier";
// reading from the basic config data
import configData from "./config";
// Reading from main config data
// import configData from "../../../config/mainConfig"


export const KayakNew: React.FC<{
// eslint-disable-next-line no-empty-pattern
}> = ({}) => {
    /**
     * #Using Remotion Video Information
     * ##frame: the exact frame that the video is runnig
     * ##durationInFrames: the duration of the video, or current component duration.
     * ##fps: speed of video
     */
    const frame = useCurrentFrame();
    const {durationInFrames, fps} = useVideoConfig();
    /**
     *#volume Effect:
     * ##This effect changes the volume of the audio or video.
     */
        // Fading the volume out,
    const volumeEffect = interpolate(
            frame,
            [durationInFrames - 60, durationInFrames],
            [1, 0]
        );
    const outroEffectTime = configData.footage.length * (configData.footage[0].endsAt - configData.footage[0].startsFrom) + 14
    //Spliting the text, for using it in a map (animating each word).
    const startTextSplit = configData.texts.startText.text.split(" ");
    return (
        <AbsoluteFill>
            {/* Mapping the audio, so if the audio was more than 1 it could handle it. */}
            {/* {configData.sfx.map((audioAddress: string, i: number) => */}
            {/*     // For each 8 seconds change the audio */}
            {/* // i > 0 ? */}
            {/* Trimming the video with passing two props */}
            <Audio
                volume={volumeEffect}
                src={configData.sfx[0].address}
                startFrom={configData.sfx[0].startsFrom}
                endAt={configData.sfx[0].endsAt}
            />
            {/* )} */}
            {/* Mapping the footage, so if the footage was more than 1 it could handle it. */}
            {configData.footage.map((eachVideo, i: number) =>
                <Sequence key={i}
                          from={eachVideo.startsFrom}
                          durationInFrames={eachVideo.endsAt - eachVideo.startsFrom}>
                    {/* Film Grain Effect applying to all videos */}
                    <FilmGrain key={i}>
                        {/* Trimming the video with passing two props */}
                        <VideoComp videoAddress={eachVideo.address}/>
                    </FilmGrain>
                </Sequence>
            )}

            {/* Sequences can shift the time for its children! */}
            {/* First text seqeunce */}
            <Sequence from={configData.texts.companyName.startsFrom}
                      durationInFrames={configData.texts.companyName.endsAt - configData.texts.companyName.startsFrom}>
                <Row style={styles.centeringRow}>
                    <Col style={styles.horizontalCentering}>
                        <Row style={{...styles.textContainer, width: "80%",}}>
                            {/* Mapping each character */}
                            {configData.texts.companyName.text.split("").map((eachChar: string, i: number) =>
                                <Col key={i} style={{
                                    ...styles.companyNameText,
                                    padding: "15px",
                                    left: `${i === 1 ? 125 : i === configData.texts.companyName.text.length ? i * 130 + 13 : i * 130}px`,
                                }}>
                                    <span key={i}
                                          style={styles.companyNameCharacter}>
                                        {eachChar}
                                    </span>
                                </Col>
                            )}
                        </Row>
                    </Col>
                </Row>
            </Sequence>
            <Sequence from={30} durationInFrames={(configData.footage.length - 1) * 30}>
                <Row style={styles.centeringRow}>
                    <Col>
                        {/* Dividing the string in to two words */}
                        {textAnimations.divideEachTwoWordsFadeAnimation(startTextSplit, 0, 1, 7, 5)}
                    </Col>
                </Row>
            </Sequence>
            <Sequence
                from={configData.texts.endText.startsFrom}
                durationInFrames={configData.texts.endText.endsAt - configData.texts.startText.startsFrom}>
                {/* Company name outro */}
                <Row style={{
                    ...styles.centeringRow,
                    backgroundColor: configData.colors.primaryColor
                }}>
                    <Col style={{...styles.horizontalCentering, marginBottom: "25%"}}>
                        <Row style={{...styles.textContainer, width: "80%"}}>
                            {configData.texts.companyName.text.split("").map((eachChar: string, i: number) =>
                                <Col key={i} style={{
                                    ...styles.companyNameText,
                                    padding: "15px",
                                    left: `${i === 1 ? 125 : i === configData.texts.companyName.text.length ? i * 130 + 13 : i * 130}px`,
                                }}>
                                    <span key={i}
                                          style={styles.companyNameCharacter}>
                                        {eachChar}
                                    </span>
                                </Col>
                            )}
                        </Row>
                    </Col>
                </Row>
                <AbsoluteFill className="justify-content-center">
                    <Row style={styles.centeringRow}>
                        <Col style={styles.endingColumn}>
                            {/* For the down scaling effect we use this condition*/}
                            {frame < outroEffectTime ?
                                <EasingBezier mainContainerCss={styles.searchInputCss} animationDuration={15}
                                              animationStartFrom={0} animationDamping={200}
                                              animationStiffness={200} scaleFrom={0} scaleTo={1.1} x1={0.8} y1={0.22}
                                              x2={0.96} y2={0.65}>
                                    <FadeAnimation mainContainerCss={styles.verticalCentering} animationStartFrom={0}
                                                   animationEndAt={15} opacityFrom={0} opacityTo={1}>
                                <span style={styles.searchIconCss}>
                                    <SearchIcon width={"50"} height={"50"}/>
                                </span>
                                        <span style={styles.searchInputText}>
                                    Search Now
                                </span>
                                    </FadeAnimation>
                                </EasingBezier>
                                :
                                <EasingBezier mainContainerCss={styles.searchInputCss} animationDuration={10}
                                              animationStartFrom={15} animationDamping={200}
                                              animationStiffness={200} scaleFrom={1.1} scaleTo={1} x1={0.8} y1={0.22}
                                              x2={0.96} y2={0.65}>
                                    <FadeAnimation mainContainerCss={styles.verticalCentering}
                                                   animationStartFrom={0} animationEndAt={15} opacityFrom={0}
                                                   opacityTo={1}>
                                        <span style={styles.searchIconCss}>
                                            <SearchIcon width={"50"} height={"50"}/>
                                        </span>
                                        <span style={styles.searchInputText}>
                                            Search Now
                                        </span>
                                    </FadeAnimation>
                                </EasingBezier>
                            }
                        </Col>
                    </Row>
                </AbsoluteFill>
            </Sequence>
        </AbsoluteFill>
    );
};
