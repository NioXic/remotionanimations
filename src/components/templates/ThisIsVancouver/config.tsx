export default {
    "savePath": "/tmp",
    "videoFormat": "square",
    "videoDuration": 450,

    // Array for looping easier
    // Start from frame ...
    // Ends at frame ...
    "footage": [

        {
            "startsFrom": 0,
            "endsAt": 90,
            "address": require("../../../assets/footages/3.mp4"),
        },
        {
            "startsFrom": 90,
            "endsAt": 180,
            "address": require("../../../assets/footages/4.mp4"),
        },
        {
            "startsFrom": 180,
            "endsAt": 270,
            "address": require("../../../assets/footages/2.mp4"),
        },        {
            "startsFrom": 270,
            "endsAt": 345,
            "address": require("../../../assets/footages/1.mp4"),
        },
        {
            "startsFrom": 345,
            "endsAt": 450,
            "address": require("../../../assets/footages/5.mp4"),
        },

    ],
    // Array for looping easier
    "sfx": [
        {
            "startsFrom": 0, // If composition is 30fps, then it will start at 0
            "endsAt": 460,  // If composition is 30fps, then it will ends at 15s
            "address": require("../../../assets/SFX/SFX3.mp3"),
        },
    ],
    "texts": {
        // It's easier to provide duration instead of end frame
        "companyName": {
            "text": "Zebracat",
            "startsFrom": 395,
            "endsAt": 460,
            "animationDuration": 35,
        },
        "startText": {
            "text": "This is a sample to see something important",
            "startsFrom": 0,
            "endsAt": 90,
            "animationDuration": 90,
        },
        "endText": {
            "text": "AI-Generated",
            "startsFrom": 395,
            "endsAt": 460,
            "animationDuration": 35,
        },
    },
    "colors": {
        "primaryColor": "#29A7B0",
        "secondaryColor": "#F3C402",
        "textPrimaryColor": "#958E27",
        "textSecondaryColor": "#F3C402",
        "textBasicColor": "white",
        // primaryColor: "#2C3333",
        // secondaryColor: "#395B64",
        // textPrimaryColor: "#A5C9CA",
        // textSecondaryColor: "#E7F6F2",
        // textBasicColor: "#E7F6F2",
    },
    "fonts": {
        "primaryFont": "FranklinGothicHeavyRegular",
        "secondaryFont": "MontserratMedium",
    }
};
