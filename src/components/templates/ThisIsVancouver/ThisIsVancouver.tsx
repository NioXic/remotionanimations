import {
    AbsoluteFill, Audio, interpolate,
    Sequence, useCurrentFrame, useVideoConfig, Easing,
} from 'remotion';
import {VideoComp} from '../../VideoComp';
import {Row, Col} from 'react-bootstrap';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import {TranslateYAnimation} from "../../animations/TranslateYAnimation";
import React from "react";
import {FadeAnimation} from "../../animations/FadeAnimation";
import {TranslateXAnimation} from "../../animations/TranslateXAnimation";
// @ts-ignore
import styles from "../../../config/styles/ThisIsVancouverStyles";
import {FadeInOutAnimation} from "../../animations/FadeInOutAnimation";
import {TransformScale} from "../../animations/TransformScale";
import {TransformHeightAnimation} from "../../animations/TransformHeightAnimation";
import {Words2D} from "../../effects/Words2D";
// reading from the basic config data
import configData from "./config";
// Reading from main config data
// import configData from "../../../config/mainConfig"

export const ThisIsVancouver: React.FC<{

// eslint-disable-next-line no-empty-pattern
}> = ({}) => {
    const frame = useCurrentFrame();
    const {durationInFrames, fps} = useVideoConfig();
    const everyFootageDuration = fps * 3;
    // Fading the volume out,
    const volumeEffect = interpolate(
        frame,
        [durationInFrames - 60, durationInFrames],
        [1, 0]
    );

    const companyNameSplited = configData.texts.companyName.text.split("");
    return (
        <AbsoluteFill>
            {/* Mapping the audio, so if the audio was more than 1 it could handle it. */}
            {/* {configData.sfx.map((audioAddress: string, i: number) => */}
            {/*     // For each 8 seconds change the audio */}
            {/* // i > 0 ? */}
            {/*     <Sequence key={i} from={i * 250}> */}
            {/* Trimming the video with passing two props */}
            <Audio
                volume={volumeEffect}
                src={configData.sfx[0].address}
                startFrom={configData.sfx[0].startsFrom}
                endAt={configData.sfx[0].endsAt}/>
            {/* </Sequence> */}
            {/* )} */}
            {/* Mapping the footage, so if the footage was more than 1 it could handle it. */}
            {/* Mapping Videos Here */}
            {configData.footage.map((eachVideo, i) =>
                <Sequence key={i}
                          from={eachVideo.startsFrom}
                          durationInFrames={eachVideo.endsAt - eachVideo.startsFrom}
                          style={{position: "relative", zIndex: "0"}}>
                    {/* Trimming the video with passing two props */}
                    {
                        // animate the last footage ending
                        i != configData.footage.length - 1 ?
                            <VideoComp videoAddress={eachVideo.address}/>
                            :
                            <FadeAnimation animationStartFrom={0} animationEndAt={10}
                                           opacityFrom={0} opacityTo={1}>
                                <FadeAnimation animationStartFrom={30} animationEndAt={50}
                                               opacityFrom={1} opacityTo={0}>
                                    <VideoComp videoAddress={eachVideo.address}/>
                                </FadeAnimation>
                            </FadeAnimation>
                    }
                </Sequence>
            )}
            <Sequence from={configData.texts.startText.startsFrom}
                      durationInFrames={configData.texts.startText.endsAt}>
                <Row style={{
                    width: "100%", marginLeft: 0,
                    position: 'absolute',
                    bottom: "20%",
                }}>
                    <Col className="col-12" style={{display: "flex", justifyContent: "center", textAlign:"center"}}>
                        <FadeAnimation animationStartFrom={configData.texts.startText.startsFrom + 10}
                                       animationEndAt={configData.texts.startText.startsFrom + 15}
                                       opacityFrom={0} opacityTo={1}>
                            <span style={styles.starterTextCSS}>
                                {configData.texts.startText.text}
                            </span>
                        </FadeAnimation>
                    </Col>
                </Row>
            </Sequence>
            {/* Giving this sequence a unqiue key */}
            <Sequence from={configData.texts.companyName.startsFrom} key={configData.footage.length + 1}>
                <div style={styles.displayFlexFillCenter}>
                    {/* Opening this section with this animation, it makes the page grow from the center */}
                    {/* Data for these animation configuration is static */}
                    <TransformHeightAnimation mainContainerCss={styles.mainPageCss}
                                              animationStartFrom={0}
                                              animationDuration={configData.texts.companyName.animationDuration}
                                              heightFrom={40} heightTo={100}
                                              heightUnit={"%"}
                                              animationDamping={200}/>
                    <Row style={{
                        width: "100%", marginLeft: 0,
                        position: 'absolute',
                        top: "10%"
                    }}>
                        <Col className="col-12" style={{display: "flex", justifyContent: "center"}}>
                            <FadeAnimation animationStartFrom={10} animationEndAt={20}
                                           opacityFrom={0} opacityTo={1}>
                                <TransformScale animationStiffness={50} mainContainerCss={styles.endingMainText}
                                                animationStartFrom={15}
                                                animationDuration={120} animationDamping={50}
                                                transformScaleFrom={1} transformScaleTo={1.17}>
                                    {configData.texts.endText.text}
                                </TransformScale>
                                <TransformScale animationStiffness={50} mainContainerCss={styles.companyNameFirstCSS}
                                                animationStartFrom={15}
                                                animationDuration={120}
                                                animationDamping={50}
                                                transformScaleFrom={1} transformScaleTo={1.17}>
                                    {configData.texts.companyName.text}
                                </TransformScale>
                            </FadeAnimation>
                        </Col>
                    </Row>
                    <FadeAnimation animationStartFrom={35} animationEndAt={45}
                                   opacityFrom={0} opacityTo={1}>
                        <Row style={{
                            width: "100%",
                            marginLeft: 0,
                            position: "absolute",
                            display: "flex",
                            justifyContent: 'center',
                            left: '0%',
                            bottom: "20%"
                        }}>
                            {companyNameSplited.map((eachWord, i) => {
                                if (i % 3 === 0 && i != companyNameSplited.length - 1) {
                                    return (
                                        <Col className="col-12"
                                             style={{display: "flex", justifyContent: "center"}}>
                                            <FadeAnimation animationStartFrom={i}
                                                           animationEndAt={i + 2} opacityFrom={0}
                                                           opacityTo={1}>
                                                <span style={{...styles.companyNameSecondCSS, position: 'unset'}}>
                                                    {`${companyNameSplited[i]} ${companyNameSplited[i + 1]} ${companyNameSplited[i + 2] ? companyNameSplited[i + 2] : ""}`}
                                                </span>
                                            </FadeAnimation>
                                        </Col>
                                    );
                                }
                                // Last Word of the text
                                if (i === companyNameSplited.length - 1 && i % 3 === 0) {
                                    return (
                                        <Col className="col-12" style={{display: "flex", justifyContent: "center"}}>
                                            <FadeAnimation animationStartFrom={i}
                                                           animationEndAt={i + 2} opacityFrom={0}
                                                           opacityTo={1}>
                                            <span style={{...styles.companyNameSecondCSS, position: 'unset'}}>
                                                {companyNameSplited[i] + " " + (companyNameSplited[i + 1] ? companyNameSplited[i + 1] : "")}
                                            </span>
                                            </FadeAnimation>
                                        </Col>

                                    );
                                    // Dividing each two words from the main text
                                }
                                // If the conditions never apply, the input is empty so return nothing.
                                return ("")
                            })}
                        </Row>
                        {/* IMPORTANT: THIS IS THE ORIGINAL VIDEO ENGING */}
                        {/* <span style={styles.companyNameSecondCSS}> */}
                        {/*     /!* This section is static *!/ */}
                        {/*     {(configData.texts.companyName.toUpperCase()).slice(0, 1)} */}
                        {/*     <span style={{color: configData.colors.textSecondaryColor, fontSize: 60}}> */}
                        {/*         ^ */}
                        {/*     </span> */}
                        {/*     {(configData.texts.companyName.toUpperCase()).slice(2, 3)} */}

                        {/*     <span style={{...styles.companyNameSecondCSS, bottom: "-80%", left: "-5%"}}> */}
                        {/*         {(configData.texts.companyName.toUpperCase()).slice(3, 4)} */}
                        {/*         <span style={{color: configData.colors.textSecondaryColor}}> */}
                        {/*         {(configData.texts.companyName.toUpperCase()).slice(4, 5)} */}
                        {/*     </span> */}
                        {/*         {(configData.texts.companyName.toUpperCase()).slice(5, 6)} */}
                        {/*     </span> */}
                        {/*     <span style={{...styles.companyNameSecondCSS, bottom: "-160%", left: "65%"}}> */}
                        {/*       <span style={styles.companyNameRotate180}> */}
                        {/*           ^ */}
                        {/*       </span> */}
                        {/*         {(configData.texts.companyName.toUpperCase()).slice(7, 9)} */}
                        {/*     </span> */}
                        {/* </span> */}
                    </FadeAnimation>
                </div>
            </Sequence>
        </AbsoluteFill>
    );
};
