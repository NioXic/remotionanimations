import {
    AbsoluteFill, Audio, interpolate,
    Sequence, useCurrentFrame, useVideoConfig,
} from 'remotion';
import {VideoComp} from '../../VideoComp';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import React from "react";
import {FadeAnimation} from "../../animations/FadeAnimation";
import {Words2D} from "../../effects/Words2D";
import styles from "../../../config/styles/KAYAKStyles";
import globalStyles from "../../../config/styles/GlobalStyles";
import textAnimations from "../../animations/Fade2DAnimation"
import companyTitle from "./CompanyTitle"
import {Row, Container, Col} from 'react-bootstrap';

// reading from the basic config data
import configData from "./config";
// Reading from main config data
// import configData from "../../../config/mainConfig"

export const KAYAK: React.FC<{
// eslint-disable-next-line no-empty-pattern
}> = ({}) => {
    /**
     * #Using Remotion Video Information
     * ##frame: the exact frame that the video is runnig
     * ##durationInFrames: the duration of the video, or current component duration.
     * ##fps: speed of video
     */
    const frame = useCurrentFrame();
    const {durationInFrames, fps} = useVideoConfig();
    /**
     *#volume Effect:
     * ##This effect changes the volume of the audio or video.
     */
        // Fading the volume out,
    const volumeEffect = interpolate(
            frame,
            [durationInFrames - 30, durationInFrames],
            [1, 0]
        );
    //Spliting the text, for using it in a map (animating each word).
    const startTextSplit = configData.texts.startText.text.split(" ");
    const middleTextSplit = configData.texts.middleText.text.split(" ");
    const endTextSplit = configData.texts.endText.text.split(" ");

    return (
        <AbsoluteFill>
            {/* Mapping the audio, so if the audio was more than 1 it could handle it. */}
            {/* {configData.sfx.map((audioAddress: string, i: number) => */}
            {/*     // For each 8 seconds change the audio */}
            {/* // i > 0 ? */}
            {/* Trimming the video with passing two props */}
            <Audio
                volume={volumeEffect}
                src={configData.sfx[0].address}
                startFrom={configData.sfx[0].startsFrom} // If composition is 30fps, then it will start at 0
                endAt={configData.sfx[0].endsAt} // If composition is 30fps, then it will ends at 10s
            />
            {/* )} */}
            {/* Mapping the footage, so if the footage was more than 1 it could handle it. */}
            {configData.footage.map((eachVideo, i: number) =>
                <Sequence key={i}
                          from={eachVideo.startsFrom}
                          durationInFrames={eachVideo.endsAt - eachVideo.startsFrom}>
                    {/* Trimming the video with passing two props */}
                    <VideoComp videoAddress={eachVideo.address}/>
                </Sequence>
            )}
            {/* Sequences can shift the time for its children! */}
            <Sequence from={configData.texts.companyName.startsFrom}
                      durationInFrames={(configData.texts.companyName.endsAt - configData.texts.companyName.startsFrom)}>
                <Row style={globalStyles.centeringRow}>
                    <Col>
                        {/* Company Title */}
                        {companyTitle.companyTitle()}
                    </Col>
                </Row>
            </Sequence>
            {/* first text part */}
            <Sequence from={configData.texts.startText.startsFrom}
                      durationInFrames={configData.texts.startText.animationDuration}>
                <Row style={globalStyles.centeringRow}>
                    <Col>
                        {/* Divide first three words of the string and animate it and make it 2D */}
                        {textAnimations.textFadeInAnimationDivide3Words(startTextSplit, 0, 1, 3, 2)}
                    </Col>
                </Row>
            </Sequence>
            <Sequence from={configData.texts.startText.endsAt - configData.texts.startText.animationDuration}
                      durationInFrames={configData.texts.startText.animationDuration}>
                <Row style={globalStyles.centeringRow}>
                    <Col>
                        {/* Divide first three words of the string and animate it and make it 2D */}
                        {textAnimations.textFadeInAnimationDivide3Words(startTextSplit, 1, 0, 3, 2)}
                    </Col>
                </Row>
            </Sequence>


            {/* middle text part */}
            <Sequence from={configData.texts.middleText.startsFrom}
                      durationInFrames={configData.texts.middleText.animationDuration}>
                <Row style={globalStyles.centeringRow}>
                    <Col>
                        {/* Divide first two words of the string and animate it and make it 2D */}
                        {textAnimations.textFadeAnimationDivide2Words(middleTextSplit, 0, 1, 3, 2)}
                    </Col>
                </Row>
            </Sequence>
            <Sequence from={configData.texts.middleText.endsAt - configData.texts.middleText.animationDuration}
                      durationInFrames={configData.texts.middleText.animationDuration}>
                <Row style={globalStyles.centeringRow}>
                    <Col>
                        {textAnimations.textFadeAnimationDivide2Words(middleTextSplit, 1, 0, 3, 2)}
                    </Col>
                </Row>
            </Sequence>


            {/* end text part */}
            <Sequence from={configData.texts.endText.startsFrom}
                      durationInFrames={configData.texts.endText.animationDuration}>
                <Row style={globalStyles.centeringRow}>
                    <Col>
                        {textAnimations.divideEachTwoWordsFadeAnimation(endTextSplit, 0, 1, 3, 2)}
                    </Col>
                </Row>
            </Sequence>
            <Sequence
                from={configData.texts.endText.endsAt - (configData.texts.endText.animationDuration - 45)}
                durationInFrames={configData.texts.endText.animationDuration - 45}>
                <Row style={globalStyles.centeringRow}>
                    <Col>
                        {textAnimations.divideEachTwoWordsFadeAnimation(endTextSplit, 1, 0, 3, 2)}
                    </Col>
                </Row>
            </Sequence>
            <Sequence from={260}>
                <Row style={globalStyles.centeringRow}>
                    <Col>
                        {companyTitle.companyTitle()}
                    </Col>
                </Row>
            </Sequence>
        </AbsoluteFill>
    );
};
