const Data = {
    savePath: "/tmp",
    // Array for looping easier
    footage: [
        {
            startsFrom: 0,
            endsAt: 15,
            address: require("../../../../assets/footages/thirdVideo1.mp4"),
        },
        {
            startsFrom: 15,
            endsAt: 30,
            address: require("../../../../assets/footages/25.mp4"),
        },
        {
            startsFrom: 30,
            endsAt: 60,
            address: require("../../../../assets/footages/22.mp4"),
        },
        {
            startsFrom: 60,
            endsAt: 90,
            address: require("../../../../assets/footages/26.mp4"),
        },
        {
            startsFrom: 90,
            endsAt: 120,
            address: require("../../../../assets/footages/23.mp4"),
        },
        {
            startsFrom: 120,
            endsAt: 150,
            address: require("../../../../assets/footages/27.mp4"),
        },
        {
            startsFrom: 150,
            endsAt: 180,
            address: require("../../../../assets/footages/20.mp4"),
        },
        {
            startsFrom: 180,
            endsAt: 240,
            address: require("../../../../assets/footages/21.mp4"),
        },
        {
            startsFrom: 240,
            endsAt: 300,
            address: require("../../../../assets/footages/25.mp4"),
        },
    ],

    // Array for looping easier
    sfx: [
        {
            startsFrom: 0, // Frames
            endsAt: 300, // Frames
            address: require("../../../../assets/SFX/SFX4.mp3"),
        }
    ],
    texts: {
        // It's easier to provide duration instead of end frame
        companyName: {
            text: "KAYAK",
            startsFrom: 0,
            endsAt: 30,
        },
        startText: {
            text: "We are all different",
            startsFrom: 50,
            endsAt: 135,
            animationDuration : 45,
        },
        middleText: {
            text: "We Travel differently",
            startsFrom: 135,
            endsAt: 185,
            animationDuration : 45,
        },
        endText: {
            text: "Find the trip that's perfect for you.",
            startsFrom: 185,
            endsAt: 255,
            animationDuration : 60,
        },
    },
    colors: {
        primaryColor: "#3A3A39",
        secondaryColor: "#6C6A40",
        textPrimaryColor: "#FFFFFFFF",
        textSecondaryColor: "#F96528",
    },
    fonts: {
        primaryFont: "MetropolisRegular",
        secondaryFont: "MontserratMedium",
    }
};
export const configData = Data;


