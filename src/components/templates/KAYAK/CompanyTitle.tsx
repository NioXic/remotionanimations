import styles from "../../../config/styles/KAYAKStyles";
import configData from "./config";
import React from "react";
import {Col, Row} from "react-bootstrap";

/**
 * This function returns the intro and outro of KAYAK sample videos.
 * this can be modified for using it for different company names if needed.
 */
// Intro and Outro Title
function companyTitle() {
    return (
        <Row style={styles.textContainer}>
            {configData.texts.companyName.text.split("").map((eachChar: string, i: number) =>
                <Col key={i} style={{
                    ...styles.companyNameText,
                    left: `${i === 1 ? 125 : i === configData.texts.companyName.text.length ? i * 130 + 13 : i * 130}px`,
                }}>
                    <span key={i}
                          style={styles.companyNameCharacter}>
                        {eachChar}
                    </span>
                </Col>
            )}
        </Row>
    );
}

export default {
    companyTitle,
}