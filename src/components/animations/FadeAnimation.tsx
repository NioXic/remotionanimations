import React from 'react';
import {AbsoluteFill, interpolate, spring, useCurrentFrame, useVideoConfig} from 'remotion';


/**
 * #Basic Fade Animation
 * ## This animation will fade (in or out based on what opacity you gave it to animate) all the elements inside it.
 * @param children:JSXElement: children inside this element
 * @param mainContainerCss:CSSProperties: main container styles
 * @param animationStartFrom:Number(Frame): the frame you want your animation to start
 * @param animationEndAt:Number(Frame): the frame you want your animation to ends at
 * @param opacityFrom:Number(0-1): the opacity you want your animation to starts with
 * @param opacityTo:Number(0-1): the opacity you want your animation to ends at
 * @constructor
 */
export const FadeAnimation: React.FC<{
    children?:
        | React.ReactChild
        | React.ReactChild[];
    mainContainerCss?: React.CSSProperties,
    animationStartFrom: number,
    animationEndAt: number,
    opacityFrom: number,
    opacityTo: number,
}> = ({
          children,
          mainContainerCss,
          animationStartFrom,
          animationEndAt,
          opacityFrom,
          opacityTo,
      }) => {
    const frame = useCurrentFrame();
    const {durationInFrames, fps} = useVideoConfig();
    // you can build your fade animation, how ever you want
    //ex: fade in => opacityFrom = 0, fade out => opacityTo = 1
    const opacity = interpolate(
        frame,
        [animationStartFrom, animationEndAt],
        [opacityFrom, opacityTo],
        {
            extrapolateLeft: 'clamp',
            extrapolateRight: 'clamp',
        }
    );

    return (
            <div style={{...mainContainerCss, opacity}}>
                {/* children of this component */}
                {children}
            </div>
    );
};
