import React from 'react';
import {AbsoluteFill, interpolate, spring, useCurrentFrame, useVideoConfig} from 'remotion';

/**
 * #Fade In Out Animation
 * ## This animation is capable to fade-in and fade-out your element with different timing (but it is mostly used for the footage begining and ending animation.
 * @param children:JSXElement: children inside this element
 * @param mainContainerCss:CSSProperties: main container styles
 * @param animationFadeInStartFrom:Number(Frame): animation fade in start from this frame
 * @param animationFadeInEndAt::Number(Frame): animation fade in ends at this frame
 * @param animationFadeOutStartFrom::Number(Frame): animation fade out start from this frame
 * @param animationFadeOutEndAt::Number(Frame): animation fade out ends at this frame
 * @param opacityFadeInFrom:Number(0-1): opacity you want your fade-in animation starts with
 * @param opacityFadeInTo:Number(0-1): opacity you want your fade-in animation ends at
 * @param opacityFadeOutFrom:Number(0-1): opacity you want your fade-out animation starts with
 * @param opacityFadeOutTo:Number(0-1): opacity you want your fade-out animation ends at
 * @constructor
 */
export const FadeInOutAnimation: React.FC<{
    children?:
        | React.ReactChild
        | React.ReactChild[];
    mainContainerCss?: React.CSSProperties,
    animationFadeInStartFrom: number,
    animationFadeInEndAt: number,
    animationFadeOutStartFrom: number,
    animationFadeOutEndAt: number,
    opacityFadeInFrom: number,
    opacityFadeInTo: number,
    opacityFadeOutFrom: number,
    opacityFadeOutTo: number,
}> = ({
          children,
          animationFadeInStartFrom,
          animationFadeInEndAt,
          animationFadeOutStartFrom,
          animationFadeOutEndAt,
          opacityFadeInFrom,
          opacityFadeInTo,
          opacityFadeOutFrom,
          opacityFadeOutTo,
      }) => {
    const frame = useCurrentFrame();
    const {durationInFrames, fps} = useVideoConfig();
    // You can build your fade animation, how ever you want
    //Fade out-in
    const opacity = interpolate(
        frame,
        [animationFadeInStartFrom, animationFadeInEndAt, animationFadeOutStartFrom, animationFadeOutEndAt],
        // v--v---v----------------------v
        [opacityFadeInFrom, opacityFadeInTo, opacityFadeOutFrom, opacityFadeOutTo],
        {
            extrapolateLeft: 'clamp',
            extrapolateRight: 'clamp',
        }
    );

    return (
        <div style={{opacity}}>
            {/* children of this component */}
            {children}
        </div>
    );
};
