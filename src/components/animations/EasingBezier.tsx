import {AbsoluteFill, useCurrentFrame, useVideoConfig, spring, interpolate, Easing} from "remotion";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
import React from "react";

/**
 * #Easing Bezier Animation
 * ##this will make every child of this element, Ease in to the screen
 * @param children:JSXElement: children inside this element
 * @param mainContainerCss:CSSProperties: main container styles
 * @param animationDuration:Number(Frames): duration of the animation
 * @param animationDamping:Number: damping of this animation
 * @param animationStiffness:Number: stiffness of this animation
 * @param animationStartFrom:Number(Frames): animation starts from this frame
 * @param scaleTo:Number: the scale that you want your elements to have
 * @param scaleFrom:Number: the scale that you want your element to starts with
 * @param x1:Number
 * @param x2:Number
 * @param y1:Number
 * @param y2:Number
 * @constructor
 */
export const EasingBezier: React.FC<{
    children?:
        | React.ReactChild
        | React.ReactChild[];
    // Styling the main container must be possible from the parent,
    mainContainerCss?: React.CSSProperties,
    animationDuration: number,
    animationStartFrom: number,
    animationDamping: number,
    animationStiffness: number,
    scaleFrom: number,
    scaleTo: number,
    x1: number,
    y1: number,
    x2: number,
    y2: number,
}> = ({
          children,
          mainContainerCss,
          animationDuration,
          animationDamping,
          animationStiffness,
          animationStartFrom,
          scaleTo,
          scaleFrom,
          x1, x2, y1, y2
      }) => {
    const frame = useCurrentFrame();
    const {fps} = useVideoConfig();

    // Making the animation smoother
    const TransformProgress = spring({
        frame: frame - animationStartFrom,
        fps,
        config: {
            damping: animationDamping,
            stiffness: animationStiffness,
        },
        durationInFrames: animationDuration,
    });
    const easingBezier = interpolate(TransformProgress, [0, 1], [scaleFrom, scaleTo], {
        easing: Easing.bezier(x1, y1, x2, y2),
        extrapolateLeft: "clamp",
        extrapolateRight: "clamp",
    });
    return (
        <div style={{
            ...mainContainerCss,
            transform: `scale(${easingBezier})`
        }}>
            {/* children of this component */}
            {children}
        </div>
    );
};