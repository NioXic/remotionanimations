import React from 'react';
import {interpolate, spring, useCurrentFrame, useVideoConfig} from 'remotion';


/**
 * #Transforming Height Animation
 * ##This animation will transform the height of the base element(Parent Child)
 * @param children:JSXElement: children inside this element
 * @param mainContainerCss:CSSProperties: main container styles
 * @param animationStartFrom:Number(Frame): the frame you want your animation to start
 * @param animationDuration:Number(Frame): the duration of the animation(in frames)
 * @param heightFrom:Number: the height you want your animation to starts with
 * @param heightTo:Number: the height you want your animation to ends at
 * @param animationDamping:Number: damping of the animation
 * @param heightUnit:String: the unit you want your height (percent, px, vh or etc.). !IMPORTANT: you must enter the symbol for example "%" for percent, "px" for pixels etc.
 * @constructor
 */
// This animation helps you, animate the height of the element.
export const TransformHeightAnimation: React.FC<{
    children?:
        | React.ReactChild
        | React.ReactChild[];
    mainContainerCss?: React.CSSProperties,
    animationStartFrom: number,
    animationDuration: number,
    heightFrom: number,
    heightTo: number,
    animationDamping: number,
    heightUnit: string,
}> = ({
          children,
          mainContainerCss,
          animationStartFrom,
          animationDuration,
          heightFrom,
          heightTo,
          animationDamping,
          heightUnit
      }) => {
    const frame = useCurrentFrame();
    const {fps} = useVideoConfig();
    const TranslationProgress = spring({
        frame: frame - animationStartFrom,
        fps,
        config: {
            damping: animationDamping,
        },
        durationInFrames: animationDuration,
    });
    // Transforming the height
    const height = interpolate(
        TranslationProgress,
        [0, 1],
        [heightFrom, heightTo],
    );

    return (
        <div style={{...mainContainerCss, height: `${height}${heightUnit}`}}>
            {/* children of this component */}
            {children}
        </div>
    );
};
