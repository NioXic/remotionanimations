import React from 'react';
import {interpolate, spring, useCurrentFrame, useVideoConfig} from 'remotion';

/**
 * #Translate X Animation
 * ##This animation will animate the child element in X Axis (Horizontal)
 * @param children:JSXElement: children inside this element
 * @param mainContainerCss:CSSProperties: main container styles
 * @param animationStartFrom:Number(Frame): the frame you want your animation to start
 * @param animationDuration:Number(Frame): the duration of the animation(in frames)
 * @param animationDamping:Number: damping of the animation
 * @param translateXAxisFrom:Number: XAxis position(coordinates) that your animation starts with
 * @param translateXAxisTo:Number: XAxis position(coordinates) that your animation ends at
 * @constructor
 */
export const TranslateXAnimation: React.FC<{
    children?:
        | React.ReactChild
        | React.ReactChild[];
    // Styling the main container must be possible from the parent,
    mainContainerCss?: React.CSSProperties,
    animationDuration: number,
    animationDamping: number,
    animationStartFrom: number,
    translateXAxisFrom: number,
    translateXAxisTo: number,
}> = ({
          children,
          mainContainerCss,
          animationDuration,
          animationDamping,
          animationStartFrom,
          translateXAxisFrom,
          translateXAxisTo
      }) => {
    const frame = useCurrentFrame();
    const {fps} = useVideoConfig();
    // For smoother transition
    const TranslationProgress = spring({
        frame: frame - animationStartFrom,
        fps,
        config: {
            damping: animationDamping,
        },
        durationInFrames: animationDuration,
    });

    const TranslateX = interpolate(
        TranslationProgress,
        [0, 1],
        [translateXAxisFrom, translateXAxisTo]
    );
    return (
            <div style={{
                ...mainContainerCss,
                transform: `translateX(${TranslateX}px)`,
            }}>
                {/* children of this component */}
                {children}
            </div>

    );
};
