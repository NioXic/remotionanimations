import styles from "../../config/styles/KAYAKStyles";
import {FadeAnimation} from "./FadeAnimation";
import {Words2D} from "../effects/Words2D";
import React from "react";
import configData from "../templates/KAYAK/config";


/**
 * This animation divides the first three words and animate them separately,
 * only first three words, the words after the first two words, will be animated one by one.
 * @param text:Array<String>: the string, that you want to apply to this animation.
 * @param opacityFrom:Number: opacity that you want your animation to start from
 * @param opacityTo:Number: opacity that you want your animation to end with.
 * @param delay:Number: the time in "frames", that you want to have as a delay for starting animation for each word
 * @param delayAfter:Number: the delay that between animating the words.
 */
function textFadeInAnimationDivide3Words(text: Array<string>, opacityFrom: number, opacityTo: number, delay: number, delayAfter: number) {
    return (
        <div style={styles.mainTextContainer}>
            {text.map((eachWord, i) => {
                const delayCalculated = i * delay;
                // Dividing first three words
                if (i === 2) {
                    return (
                        <FadeAnimation mainContainerCss={styles.textAlignCenter} animationStartFrom={delayCalculated}
                                       animationEndAt={delayCalculated + delayAfter} opacityFrom={opacityFrom}
                                       opacityTo={opacityTo}>
                            <Words2D key={i}
                                     baseCSS={styles.mainTextStyle2DTransform}
                                     secondCSS={styles.secondTextStyle2DTransform}
                                     text={`${text[0]} ${text[1]} ${text[2]}`}/>
                        </FadeAnimation>
                    );
                }
                if (i > 2) {
                    return (
                        <FadeAnimation animationStartFrom={delay}
                                       animationEndAt={delay + 5} opacityFrom={opacityFrom}
                                       opacityTo={opacityTo}>
                            <Words2D key={i} text={eachWord}
                                     baseCSS={styles.mainTextStyle2DTransform}
                                     secondCSS={styles.secondTextStyle2DTransform}/>
                        </FadeAnimation>
                    );
                }
                // If the conditions never apply, the input is empty so return an empty string.
                return ("");
            })}
        </div>

    );
}

/**
 * This animation divides the first two words, and animate them separately.
 * only first two words, the words after the first two words, will be animated one by one.
 *  * @param text:Array<String>: the string, that you want to apply to this animation.
 *  * @param opacityFrom:Number: opacity that you want your animation to start from
 *  * @param opacityTo:Number: opacity that you want your animation to end with.
 *  * @param delay:Number: the time in "frames", that you want to have as a delay for starting animation for each word
 *  * @param delayAfter:Number: the delay that between animating the words.
 *  */
function textFadeAnimationDivide2Words(text: Array<string>, opacityFrom: number, opacityTo: number, delay: number, delayAfter: number) {
    return (
        <div style={styles.mainTextContainer}>
            {text.map((eachWord, i) => {
                const delayCalculated = i * delay;
                // Dividing each two words from the main text
                if (i === 1) {
                    return (
                        <FadeAnimation mainContainerCss={styles.textAlignCenter} animationStartFrom={delayCalculated}
                                       animationEndAt={delayCalculated + delayAfter} opacityFrom={opacityFrom}
                                       opacityTo={opacityTo}>
                            <Words2D key={i}

                                     baseCSS={styles.mainTextStyle2DTransform}
                                     secondCSS={styles.secondTextStyle2DTransform}
                                     text={`${text[0]} ${text[1]}`}/>
                        </FadeAnimation>
                    );
                }
                if (i > 1) {
                    return (
                        <FadeAnimation animationStartFrom={delay}
                                       animationEndAt={delay + 5} opacityFrom={opacityFrom}
                                       opacityTo={opacityTo}>
                            <Words2D key={i} text={eachWord}
                                     baseCSS={styles.mainTextStyle2DTransform}
                                     secondCSS={styles.secondTextStyle2DTransform}/>
                        </FadeAnimation>
                    );
                }
                // If the conditions never apply, the input is empty so return an empty string.
                return ("");
            })}
        </div>

    )
}


/**
 * This animation, will divide the string by every two words, and animate them separately,
 * Dividing by every two words.
 * @param text:Array<String>: the string, that you want to apply to this animation.
 * @param opacityFrom:Number: opacity that you want your animation to start from
 * @param opacityTo:Number: opacity that you want your animation to end with.
 * @param delay:Number: the time in "frames", that you want to have as a delay for starting animation for each word
 * @param delayAfter:Number: the delay that between animating the words.
 */
function divideEachTwoWordsFadeAnimation(text: Array<string>, opacityFrom: number, opacityTo: number, delay: number, delayAfter: number) {
    return (
        <div style={styles.mainTextContainer}>
            {text.map((eachWord, i) => {
                // Last Word of the text
                const delayCalculated = i * delay;
                if (i === text.length - 1) {
                    return (
                        <FadeAnimation mainContainerCss={styles.textAlignCenter} animationStartFrom={delayCalculated}
                                       animationEndAt={delayCalculated + delayAfter} opacityFrom={opacityFrom}
                                       opacityTo={opacityTo}>
                            <Words2D
                                key={i}
                                baseCSS={styles.mainTextStyle2DTransform}
                                secondCSS={styles.secondTextStyle2DTransform}
                                text={`${text[i]}`}/>
                        </FadeAnimation>
                    );
                    // Dividing each two words from the main text
                }
                if (i % 2 === 0) {
                    return (
                        <FadeAnimation mainContainerCss={styles.textAlignCenter} animationStartFrom={delayCalculated}
                                       animationEndAt={delayCalculated + delayAfter} opacityFrom={opacityFrom}
                                       opacityTo={opacityTo}>
                            <Words2D
                                key={i}
                                baseCSS={styles.mainTextStyle2DTransform}
                                secondCSS={styles.secondTextStyle2DTransform}
                                text={`${text[i]} ${text[i + 1]}`}/>
                        </FadeAnimation>
                    );
                }
                // If the conditions never apply, the input is empty so return an empty string.
                return ("")
            })}
        </div>
    )
}

// Exporting them...
export default {
    textFadeInAnimationDivide3Words,
    textFadeAnimationDivide2Words,
    divideEachTwoWordsFadeAnimation,
}