import React from 'react';
import {interpolate, useCurrentFrame, useVideoConfig, Easing, spring} from 'remotion';

/**
 * #Transform Scale Animation
 * ##This animation will transform the scale of the child
 * @param children:JSXElement: children inside this element
 * @param mainContainerCss:CSSProperties: main container styles
 * @param animationStartFrom:Number(Frame): the frame you want your animation to start
 * @param animationDuration:Number(Frame): the duration of the animation(in frames)
 * @param transformScaleFrom:Number: the scale that you want your animation to starts with
 * @param transformScaleTo:Number: the scale that you want your animation to ends at
 * @param animationDamping:Number: animation damping
 * @param animationStiffness:Number: animation stiffness
 * @constructor
 */
export const TransformScale: React.FC<{
    children?:
        | React.ReactChild
        | React.ReactChild[];
    // Styling the main container must be possible from the parent,
    mainContainerCss?: React.CSSProperties,
    animationStartFrom: number,
    animationDuration: number,
    transformScaleFrom: number,
    transformScaleTo: number,
    animationDamping: number,
    animationStiffness: number,
}> = ({
          children,
          mainContainerCss,
          animationDuration,
          animationStartFrom,
          transformScaleFrom,
          transformScaleTo,
          animationDamping,
          animationStiffness
      }) => {

    const frame = useCurrentFrame();
    const {fps} = useVideoConfig();
    // For smoother transform
    const TransformProgress = spring({
        frame: frame - animationStartFrom,
        fps,
        config: {
            damping: animationDamping,
            stiffness: animationStiffness
        },
        durationInFrames: animationDuration,
    });

    const interpolated = interpolate(TransformProgress, [0, 1], [transformScaleFrom, transformScaleTo], {
        extrapolateLeft: "clamp",
        extrapolateRight: "clamp",
    });
    return (
        <div style={{...mainContainerCss, transform: `scale(${interpolated}, ${interpolated})`}}>
            {/* children of this component */}
            {children}
        </div>

    );
};
