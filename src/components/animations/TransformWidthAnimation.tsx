import React from 'react';
import {interpolate, spring, useCurrentFrame, useVideoConfig} from 'remotion';


/**
 * #Transforming width Animation
 * ##This animation will transform the width of the base element(Parent Child)
 * @param children:JSXElement: children inside this element
 * @param mainContainerCss:CSSProperties: main container styles
 * @param animationStartFrom:Number(Frame): the frame you want your animation to start
 * @param animationDuration:Number(Frame): the duration of the animation(in frames)
 * @param widthFrom:Number: the width you want your animation to starts with
 * @param widthTo:Number: the width you want your animation to ends at
 * @param animationDamping:Number: damping of the animation
 * @param widthUnit:String: the unit you want your width (percent, px, vh or etc.). !IMPORTANT: you must enter the symbol for example "%" for percent, "px" for pixels etc.
 * @constructor
 */
// This animation helps you, animate the width of the element (in percent).
export const TransformWidthAnimation: React.FC<{
    children?:
        | React.ReactChild
        | React.ReactChild[];
    mainContainerCss?: React.CSSProperties,
    animationStartFrom: number,
    animationDuration: number,
    widthFrom: number,
    widthTo: number,
    animationDamping: number,
    widthUnit: string,
}> = ({
          children,
          mainContainerCss,
          animationStartFrom,
          animationDuration,
          widthFrom,
          widthTo,
          animationDamping,
          widthUnit
      }) => {
    const frame = useCurrentFrame();
    const {fps} = useVideoConfig();
    const TranslationProgress = spring({
        frame: frame - animationStartFrom,
        fps,
        config: {
            damping: animationDamping,
        },
        durationInFrames: animationDuration,
    });
    // Transforming the width
    const width = interpolate(
        TranslationProgress,
        [0, 1],
        [widthFrom, widthTo],
    );

    return (
        <div style={{...mainContainerCss, width: `${width}${widthUnit}`}}>
            {/* children of this component */}
            {children}
        </div>
    );
};
