import {AbsoluteFill, useCurrentFrame, useVideoConfig, spring, interpolate} from "remotion";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
import React from "react";

/**
 * #Translate Y Animation
 * ##This animation will animate the child element in Y Axis (Vertical)
 * @param children:JSXElement: children inside this element
 * @param mainContainerCss:CSSProperties: main container styles
 * @param animationStartFrom:Number(Frame): the frame you want your animation to start
 * @param animationDuration:Number(Frame): the duration of the animation(in frames)
 * @param animationDamping:Number: damping of the animation
 * @param translateYAxisFrom:Number: YAxis position(coordinates) that your animation starts with
 * @param translateYAxisTo:Number: YAxis position(coordinates) that your animation ends at
 * @constructor
 */
export const TranslateYAnimation: React.FC<{
    children?:
        | React.ReactChild
        | React.ReactChild[];
    // Styling the main container must be possible from the parent,
    mainContainerCss?: React.CSSProperties,
    animationDuration: number,
    animationDamping: number,
    animationStartFrom: number,
    translateYAxisFrom: number,
    translateYAxisTo: number,
}> = ({
          children,
          mainContainerCss,
          animationDuration,
          animationDamping,
          animationStartFrom,
          translateYAxisFrom,
          translateYAxisTo
      }) => {
    const frame = useCurrentFrame();
    const {durationInFrames, fps} = useVideoConfig();
    // For smoother transition
    const TranslationProgress = spring({
        frame: frame - animationStartFrom,
        fps,
        config: {
            damping: animationDamping,
        },
        durationInFrames: animationDuration,
    });
    const TranslateY = interpolate(
        TranslationProgress,
        [0, 1],
        [translateYAxisFrom, translateYAxisTo]
    );
    return (
            <div style={{...mainContainerCss, transform: `translateY(${TranslateY}px)`}}>
                {/* children of this component */}
                {children}
            </div>
    );
};